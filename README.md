# README #

Testing Tls

### What to do? ###

* Add an entry for hostname in /etc/hosts (like obsidium.co)
* Create ssl.conf file with following contents
```
[req]
distinguished_name=dn
[ dn ]
CN=obsidium.co
[ ext ]
basicConstraints=UK:FALSE,pathlen:0
subjectAltName = @alt_names
[alt_names]
DNS.1 = obsidium.co
```
* openssl req -nodes -x509 -newkey rsa:2048 -config ssl.conf  -subj /C=UK/ST=LDN/L=LDN/O=Obsidium/CN=obsidium.co -keyout obsidium.co.key -out obsidium.co.crt -days 365
* openssl pkcs12 -export -nodes -inkey obsidium.co.key -in obsidium.co.crt -out certificate.p12
* sudo cp obsidium.co.crt /usr/share/ca-certificates/.
* sudo dpkg-reconfigure ca-certificates
