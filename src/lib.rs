extern crate serde_json as json;

pub type OArg = Option<json::Value>;
pub type OReturn = json::Value;

pub type OResult = Result<OReturn, String>;

pub type OAction = fn(OArg) -> OReturn;
pub type OExec = fn(&String, OArg) -> OReturn;
